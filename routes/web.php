<?php

Route::get('/', 'ArticleController@index')->name('home');

Route::get('register', 'RegisterController@showForm')->name('register');
Route::post('register', 'RegisterController@register');
Route::get('login', 'LoginController@showForm')->name('login');
Route::post('login', 'LoginController@login');
Route::get('logout', 'LoginController@logout')->name('logout');

Route::resource('article', 'ArticleController');
Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');
Route::put('profile', 'ProfileController@update')->name('profile.update');
Route::get('profile/{user}', 'ProfileController@show')->name('profile.show');
