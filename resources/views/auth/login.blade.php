@extends('templates.default')

@section('masthead')
    @component('templates.components._masthead', [
        'image' => asset('assets/img/contact-bg.jpg'),
        'title' => 'Login',
        'subtitle' => 'Login & Share your article'
    ])

    @endcomponent
@endsection

@section('content')
    <p>Mau berbagi artikelmu? Silakan Login!!</p>
    <form name="sentMessage" id="loginForm" method="post">
        @csrf
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <label>Email Address</label>
            <input type="email" name="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
            @if ($errors->has('email'))
              <p class="help-block text-danger">{{ $errors->first('email') }}</p>
            @endif
          </div>
        </div>
        <div class="control-group">
          <div class="form-group col-xs-12 floating-label-form-group controls">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" id="password" required data-validation-required-message="Please enter your password.">
            @if ($errors->has('password'))
            <p class="help-block text-danger">{{ $errors->first('password') }}</p>
          @endif
          </div>
        </div>
        <br>
        <div id="success"></div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" id="LoginButton">Login</button>
        </div>
    </form>
@endsection