@extends('templates.default')

@section('masthead')
    @component('templates.components._masthead', [
        'image' => asset('assets/img/contact-bg.jpg'),
        'title' => 'Register',
        'subtitle' => 'Create an account & Share your article'
    ])

    @endcomponent
@endsection

@section('content')
    <p>Buat akunmu!</p>
    <form action="" name="sentMessage" id="registerForm" method="post">
        @csrf
        <div class="control-group">
            <div class="form-group floating-label-form-group controls">
                <label>Name</label>
                <input type="text" name="name" class="form-control" placeholder="Your Name" id="name" required data-validation-required-message="Please enter your name." value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <p class="help-block text-danger">{{ $errors->first('name') }}</p>
                @endif
            </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <label>Email Address</label>
            <input type="email" name="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address." value="{{ old('email') }}">
            @if ($errors->has('email'))
              <p class="help-block text-danger">{{ $errors->first('email') }}</p>
            @endif
          </div>
        </div>
        <div class="control-group">
          <div class="form-group col-xs-12 floating-label-form-group controls">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" id="password" required data-validation-required-message="Please enter your password.">
            @if ($errors->has('password'))
              <p class="help-block text-danger">{{ $errors->first('password') }}</p>
            @endif
          </div>
        </div>
        <br>
        <div id="success"></div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" id="LoginButton">Register</button>
        </div>
    </form>
@endsection