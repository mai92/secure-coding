
<!DOCTYPE html>
<html lang="en">

  @include('templates.partials._head')

  <body>

    <!-- Navigation -->
    @include('templates.partials._navigation')

    <!-- Page Header -->
    @yield('masthead')

    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">

          @yield('content')
          <hr>
          <!-- Pager -->
          {{-- <div class="clearfix">
            <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
          </div> --}}
        </div>
      </div>
    </div>

    <hr>

    <!-- Footer -->
    @include('templates.partials._footer')

  </body>

</html>
