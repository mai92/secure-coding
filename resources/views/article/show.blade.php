@extends('templates.default')

@section('masthead')
    @component('templates.components._masthead', [
        'image' => asset('assets/uploads/'.$article->image),
        'title' => $article->title,
        'subtitle' => ''
    ])
        <span class="meta">Posted by
                <a href="#">
                    {{ $article->name }}
                </a>
                on
                {{ $article->created_at }}
        </span>
    @endcomponent
@endsection

@section('content')
<article>
    <div class="container">
        <div class="row">
            <p>{{ $article->body }}</p>
        </div>
    </div>
</article>
@endsection