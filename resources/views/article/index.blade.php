@extends('templates.default')

@section('masthead')
    @component('templates.components._masthead', [
        'image' => asset('assets/img/contact-bg.jpg'),
        'title' => 'List of Articles',
        'subtitle' => 'Best article for you'
    ])

    @endcomponent
@endsection

@section('content')
    @foreach ($articles as $article)
        <div class="post-preview">
            <a href="{{ route('article.show', $article) }}">
                <h2 class="post-title">
                    {{ $article->title }}
                </h2>
                <h3 class="post-subtitle">
                    {{ str_limit($article->body, 100)}}
                </h3>
            </a>
            <p class="post-meta">Posted by
            <a href="{{ route('profile.show', $article->user) }}">{{ $article->user->name }}</a>
            on {{ $article->created_at }}</p>
        </div>
        @if ($article->user_id === session('id'))
        <div class="row">
            <a href="{{ route('article.edit', $article) }}" class="btn btn-warning">Edit</a>
            <button id="delete" href="{{ route('article.destroy', $article) }}" class="btn btn-danger">Delete</button>
        </div>
        @endif
        <hr>
        <form action="" id="deleteForm" method="post">
            @csrf
            @method("DELETE")
            <input type="submit" value="" style="display:none">
        </form>
    @endforeach
@endsection

@push('scripts')
<script>



    $('button#delete').on('click', function() {
        var href = $(this).attr('href');

        document.getElementById('deleteForm').action = href;
        document.getElementById('deleteForm').submit();
    });
</script>
@endpush