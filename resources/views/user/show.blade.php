@extends('templates.default')

@section('masthead')
    @component('templates.components._masthead', [
        'image' => asset('assets/img/contact-bg.jpg'),
        'title' => 'Profile '  . $user->name,
        'subtitle' => 'Articles from '  . $user->name,
    ])

    @endcomponent
@endsection

@section('content')
    @foreach ($articles as $article)
    {{-- {{ dd($article)}} --}}
        <div class="post-preview">
            <a href="{{ route('article.show', $article->id) }}">
                <h2 class="post-title">
                    {{ $article->title }}
                </h2>
                <h3 class="post-subtitle">
                    {{ str_limit($article->body, 100)}}
                </h3>
            </a>
            <p class="post-meta">Posted by
            <a href="{{ route('profile.show', $article->user_id) }}">{{ $article->name }}</a>
            on {{ $article->created_at }}</p>
        </div>
        @if ($article->user_id === session('id'))
        <div class="row">
            <a href="{{ route('article.edit', $article->id) }}" class="btn btn-warning">Edit</a>
            <button id="delete" href="{{ route('article.destroy', $article->id) }}" class="btn btn-danger">Delete</button>
        </div>
        @endif
        <hr>
        <form action="" id="deleteForm" method="post">
            @csrf
            @method("DELETE")
            <input type="submit" value="" style="display:none">
        </form>
    @endforeach
@endsection

@push('scripts')
<script>



    $('button#delete').on('click', function() {
        var href = $(this).attr('href');

        document.getElementById('deleteForm').action = href;
        document.getElementById('deleteForm').submit();
    });
</script>
@endpush