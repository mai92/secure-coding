@extends('templates.default')

@section('masthead')
    @component('templates.components._masthead', [
        'image' => asset('assets/img/contact-bg.jpg'),
        'title' => 'Update Profile '. $user->name,
        'subtitle' => 'Update your Profile'
    ])

    @endcomponent
@endsection

@section('content')
    <p>Update your Profile</p>
    <form action="{{ route('profile.update') }}" name="sentMessage" id="registerForm" method="post" enctype="multipart/form-data">
        @csrf
        @method("PUT")
        <div class="control-group">
            <div class="form-group floating-label-form-group controls">
                <label>Name</label>
                <input type="text" name="name" class="form-control" placeholder="Your title" id="title" required data-validation-required-message="Please enter your name." value="{{ old('name') ?? $user->name }}">
                @if ($errors->has('name'))
                    <p class="help-block text-danger">{{ $errors->first('name') }}</p>
                @endif
            </div>
        </div>
        <div class="control-group">
            <div class="form-group floating-label-form-group controls">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="Your title" id="title" required data-validation-required-message="Please enter your email." value="{{ old('email') ?? $user->email }}">
                @if ($errors->has('email'))
                    <p class="help-block text-danger">{{ $errors->first('email') }}</p>
                @endif
            </div>
        </div>
        <div class="control-group">
            <div class="form-group floating-label-form-group controls">
                <label>Title</label>
                <input type="password" name="password" class="form-control" placeholder="Your pasword" id="title" >
                @if ($errors->has('password'))
                    <p class="help-block text-danger">{{ $errors->first('password') }}</p>
                @endif
            </div>
        </div>
        <br>
        <div id="success"></div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" id="">Update</button>
        </div>
    </form>
@endsection