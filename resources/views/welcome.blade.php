
<!DOCTYPE html>
<html lang="en">

  @include('templates.partials._head')

  <body>

    <!-- Navigation -->
    @include('templates.partials._navigation')

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('{{ asset('assets/img/home-bg.jpg') }}')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>Secure Coding</h1>
              <span class="subheading">Blog Percobaan untuk Workshop Secure Coding</span>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">

          @yield('content')
          {{-- <div class="post-preview">
                <a href="post.html">
                  <h2 class="post-title">
                    Man must explore, and this is exploration at its greatest
                  </h2>
                  <h3 class="post-subtitle">
                    Problems look mighty small from 150 miles up
                  </h3>
                </a>
                <p class="post-meta">Posted by
                  <a href="#">Start Bootstrap</a>
                  on September 24, 2018</p>
              </div>
              <hr> --}}
          <hr>
          <!-- Pager -->
          {{-- <div class="clearfix">
            <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
          </div> --}}
        </div>
      </div>
    </div>

    <hr>

    <!-- Footer -->
    @include('templates.partials._footer')

  </body>

</html>
