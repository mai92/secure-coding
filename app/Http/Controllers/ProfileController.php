<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfileController extends Controller
{
    public function show($id)
    {
        $user = DB::table('users')
            ->select('*')
            ->whereRaw('users.id = ' .$id)
            ->first();

        $articles = DB::table('articles')
            ->select('*')
            ->join('users', 'articles.user_id', 'users.id')
            ->whereRaw('user_id = ' .$id)
            ->get();

        return view('user.show', compact('user', 'articles'));
    }

    public function edit()
    {
        $user = DB::table('users')
            ->select('*')
            ->whereRaw('users.id = ' . session('id'))
            ->first();

        return view('user.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $user = DB::table('users')
            ->select('*')
            ->whereRaw('users.id = ' . session('id'))
            ->first();

        DB::update("UPDATE
            `users` SET name=?,email=?,password=?,updated_at=? WHERE id=?",[
            $request->name,$request->email,$user->password ?? $request->password,now(),session('id')
        ]);

        return back();
    }
}
