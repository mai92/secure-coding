<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::latest()->paginate(5);

        return view('article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!session('email')) {
            return redirect()->route('login');
        }

        $path = null;

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('images');
        }
        $userId = session('id');

        $article = DB::insert("INSERT INTO
                        `articles` (user_id,title,image,body,created_at,updated_at)
                        VALUES (?,?,?,?,?,?)",[
                        $userId, $request->title,$path,$request->body,now(),now()
                    ]);

        return redirect()->route('article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = DB::table('articles')
                        ->select('articles.*', 'users.name')
                        ->join('users', 'articles.user_id', 'users.id')
                        ->whereRaw('articles.id = ' .$id)
                        ->first();

        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('article.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!session('email')) {
            return redirect()->route('login');
        }

        $article = DB::table('articles')
                        ->select('articles.*', 'users.name')
                        ->join('users', 'articles.user_id', 'users.id')
                        ->whereRaw('articles.id = ' .$id)
                        ->first();

        $path = $article->image;

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('images');
        }

        $userId = session('id');

        $article = DB::update("UPDATE
                        `articles` SET user_id=?,title=?,image=?,body=?,updated_at=? WHERE id=?",[
                        $userId, $request->title,$path,$request->body,now(),$id
                    ]);

        return redirect()->route('article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return redirect()->route('article.index');
    }

}
