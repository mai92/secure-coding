<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class RegisterController extends Controller
{
    public function showForm()
    {
        if (session('email')) {
            return redirect('/');
        }

        return view('auth.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required'
        ]);

        $register = DB::insert("INSERT INTO users (name,email,password) VALUES (?,?,?)", [
            $request->name,$request->email,md5($request->password)
        ]);

        if ($register) {
            $user = DB::table('users')->select('id','name','email')->where('email', $request->email)->first();

            session((array) $user);

            return redirect('/');
        }
    }
}
