<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LoginController extends Controller
{
    public function showForm()
    {
        if (session('email')) {
            return redirect('/');
        }

        return view('auth.login');
    }

    public function login(Request $request)
    {
        $user = DB::table('users')
                    ->select('id','name','email')
                    ->where('email', $request->email)
                    ->where('password', md5($request->password))
                    ->first();

        session((array) $user);

        return redirect('/');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect()->route('home');
    }
}
